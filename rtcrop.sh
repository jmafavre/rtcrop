#!/bin/bash



function random_rotate {
  i_image=$1
  o_image=$2
  angle=$3
  
  w=$(convert $i_image -format "%[w]" info:)
  h=$(convert $i_image -format "%[h]" info:)

  TMP=$(mktemp /tmp/rot-XXXXX --suffix .png)
  convert $i_image -rotate $angle +repage $TMP
  
  crop_params=$(./compute_crop.py $w $h $angle)
  convert $TMP -gravity Center -crop $crop_params+0+0 +repage $o_image
  rm -f $TMP
}


if [ "$#" -ne "8" ]; then
  echo "Extract subsamples of a pair of images with rotations and translations"
  echo "Usage: ./rtcrop.sh IMG1 IMG2 SIZEX SIZEY OUTPREFIX1 OUTPREFIX2 NBANGLES"
  echo "                   NBCROP"
  echo " Parameters:"
  echo " IMG1         First input image"
  echo " IMG2         Second input image"
  echo " SIZEX        Width of the subsamples (in pixels)"
  echo " SIZEY        Height of the subsamples (in pixels)"
  echo " OUTPREFIX1   Prefix of the subsamples extracted from the first image"
  echo " OUTPREFIX2   Prefix of the subsamples extracted from the second image"
  echo " NBANGLES     Number of different angles"
  echo " NBCROP       Number of samples per angle"
  echo " "
  echo "Example: "
  echo "  ./rtcrop.sh img1.png img2.png 62 32 /tmp/test1 /tmp/test2 50 20"
  echo "will produce 2000 images (62x32 pixels) named as following:"
  echo "  /tmp/test1-000000.png"
  echo "  /tmp/test2-000000.png"
  echo "  /tmp/test1-000001.png"
  echo "  /tmp/test2-000001.png"
  echo "  ..."
  echo "  /tmp/test1-000999.png"
  echo "  /tmp/test2-000999.png"
  exit 0
fi

INPUTIMAGE1=$1
INPUTIMAGE2=$2
SIZEX=$3
SIZEY=$4
OUTPUTPREFIX1=$5
OUTPUTPREFIX2=$6
NBANGLES=$7
NBCROP=$8


width1=$(convert $INPUTIMAGE1 -format "%[w]" info:)
height1=$(convert $INPUTIMAGE1 -format "%[h]" info:)
width2=$(convert $INPUTIMAGE2 -format "%[w]" info:)
height2=$(convert $INPUTIMAGE2 -format "%[h]" info:)

if [ "$width1" -ne "$width2" ] || [ "$height1" -ne "$height2" ]; then
  echo "The two input images should have the same size:"
  echo "  ${width1}x${height1} vs ${width2}x${height2}"
  echo "Abort."
  
  exit 1
fi

IMGTMP1=$(mktemp /tmp/img-XXXXX --suffix .png)
IMGTMP2=$(mktemp /tmp/img-XXXXX --suffix .png)

i=0
for a in $(seq 1 $NBANGLES); do 
  angle=$((RANDOM % 360))
  random_rotate $INPUTIMAGE1 $IMGTMP1 $angle
  random_rotate $INPUTIMAGE2 $IMGTMP2 $angle
  width=$(convert $IMGTMP1 -format "%[w]" info:)
  height=$(convert $IMGTMP1 -format "%[h]" info:)
  if [ "$width" -gt "$SIZEX" ] || [ "$height" -gt "$SIZEX" ]; then
    for c in $(seq 1 $NBCROP); do
      output1=$OUTPUTPREFIX1-$(printf "%06d\n" $i).png
      output2=$OUTPUTPREFIX2-$(printf "%06d\n" $i).png
      x=$((RANDOM % ($width - $SIZEX)))
      y=$((RANDOM % ($height - $SIZEY)))
      convert $IMGTMP1 -crop ${SIZEX}x${SIZEY}+${x}+${y} $output1
      convert $IMGTMP2 -crop ${SIZEX}x${SIZEY}+${x}+${y} $output2
      i=$((i+1))
    done
  fi
done
